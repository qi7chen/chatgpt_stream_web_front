FROM node:18-alpine as builder

WORKDIR /app
COPY . /app
RUN npm install pnpm -g

RUN pnpm install
COPY .env.example .env
RUN pnpm run build

FROM nginx:1.25-alpine

COPY --from=builder /app/dist /usr/share/nginx/html

