import { ref } from 'vue'
import { myMSALObj } from './msalConfig'
import { useAuthStoreWithout } from '@/store/modules/auth'
import { router } from '@/router'


export function useAuth() {
  const authStore = useAuthStoreWithout();
  const isAuthenticated = ref(authStore.user !== null)

  const login = async () => {
    try {
      // Check if MSAL is initialized before using it
      if (!myMSALObj) {
        throw new Error('MSAL not initialized. Call initializeMsal() before using MSAL API.')
      }
      await myMSALObj.loginRedirect()
      isAuthenticated.value = true

      const loginResponse = await myMSALObj.loginRedirect()
      isAuthenticated.value = true
      console.log('Login success:', loginResponse)
      authStore.user = myMSALObj.getAllAccounts()[0]
      await router.push({path:'/chat'}) // 登录后跳转到聊天页面
    } catch (error) {
      console.error('Login error:', error)
    }
  }

  const logout = () => {
    if (!myMSALObj) {
      throw new Error('MSAL not initialized. Call initializeMsal() before using MSAL API.')
    }
    myMSALObj.logoutRedirect()
    isAuthenticated.value = false
    console.log('Logged out')
  }

  const handleRedirect = async () => {
    try {
      await myMSALObj.handleRedirectPromise()
      authStore.user = myMSALObj.getAllAccounts()[0]
    } catch (error) {
      console.error('Redirect error:', error)
    }
  }

  return { isAuthenticated, login, logout, handleRedirect }
}
