import { PublicClientApplication, type AccountInfo, type RedirectRequest } from '@azure/msal-browser'
import { reactive } from 'vue';

export const msalConfig = {
  auth: {

    // imperial
    clientId: '8b1b09d4-3210-42ea-96e1-7e45185d0f33',
    authority: 'https://login.microsoftonline.com/2b897507-ee8c-4575-830b-4f8267c3d307',

    // redirectUri: 'http://localhost:1002', // Replace with your actual redirect URI
  },
  cache: {
    cacheLocation: 'sessionStorage', // This configures where your cache will be stored
    storeAuthStateInCookie: false
  }
}

export const graphScopes: RedirectRequest = {
  scopes: ['user.read', 'openid', 'profile']
};
export const state = reactive({
  isAuthenticated: false,
  user: null as AccountInfo | null
});

export const myMSALObj = new PublicClientApplication(msalConfig)


