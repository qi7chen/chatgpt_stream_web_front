import type { Router } from 'vue-router'
import { useAuthStoreWithout } from '@/store/modules/auth'

function isAuthenticated() {
  const authStore = useAuthStoreWithout();
  return authStore.user !== null;
}

export function setupPageGuard(router: Router) {
  router.beforeEach(async (to, from, next) => {
    if (to.name !== 'Login' && !isAuthenticated()) {
      next({ name: 'Login' })
    } else {
      next()
    }
  })
}
