import type { AxiosProgressEvent, GenericAbortSignal } from 'axios'
import { get, post, _delete } from '@/utils/request'
import { useSettingStore } from '@/store'
import { useAuthStoreWithout } from '@/store/modules/auth'

export function fetchChatAPI<T = any>(
  prompt: string,
  options?: { conversationId?: string; parentMessageId?: string },
  signal?: GenericAbortSignal,
) {
  return post<T>({
    url: '/chat',
    data: { prompt, options },
    signal,
  })
}

export function fetchChatConfig<T = any>() {
  return post<T>({
    url: '/config',
  })
}

export function fetchChatAPIProcess<T = any>(
  params: {
    prompt: string
    options?: { conversationId?: string; parentMessageId?: string }
    signal?: GenericAbortSignal
    onDownloadProgress?: (progressEvent: AxiosProgressEvent) => void },
) {
  const settingStore = useSettingStore()
  return post<T>({
    url: '/chat-process',
    data: { prompt: params.prompt, options: params.options, systemMessage: settingStore.systemMessage },
    signal: params.signal,
    onDownloadProgress: params.onDownloadProgress,
  })
}

export function loginSession<T>(username: string, password: string): any {
    return post<T>({
      url: '/login',
        data: { username, password },
    })
}

export function fetchSession<T>() {
  return Promise.resolve({data: {auth:true}}) // 在权限认证的协议接口直接返回true
  // return post<T>({
  //   url: '/session',
  // })
}

export function fetchVerify<T>(token: string) {
    return Promise.resolve({})  // 在权限认证的协议接口直接返回true
  // return post<T>({
  //   url: '/verify',
  //   data: { token },
  // })
}

export function fetchChatConversations<T = any>() {
  return get<T>({url: '/chat'})
}

export function removeConversation<T>(uuid: number) {
    return _delete<T>({
        url: '/chat',
        data: {'conversation_id': uuid},
    })
}