## chatgpt流式调用，前端实现打印机效果

后端项目地址：https://github.com/aixiaoxin123/chatgpt_stream_web_service


## 前置要求

### Node

`node` 需要 `^16 || ^18 || ^19` 版本（`node >= 14` 需要安装 [fetch polyfill](https://github.com/developit/unfetch#usage-as-a-polyfill)），使用 [nvm](https://github.com/nvm-sh/nvm) 可管理本地多个 `node` 版本

```shell
node -v
```

### PNPM
如果你没有安装过 `pnpm`
```shell
npm install pnpm -g
```



## 本地部署安装

### 安装依赖包

1、根目录运行以下命令，安装pnpm

```
npm install pnpm -g
```


2、根目录运行以下命令,安装依赖包
```
pnpm install
```
3、测试环境运行

```
pnpm run dev 
```

4、打包生成dist文件夹

```
pnpm build
```
5、然后将 `dist` 文件夹内的文件复制到你网站服务的根目录下


6、运行地址
http://localhost:1002/



## 利用docker部署

### 使用 Docker

#### Docker 参数示例

![docker](./docs/docker.png)

#### Docker build & Run

```bash

# 后台运行
docker compose up -d --build

# 运行地址
http://localhost:3002/

# push到docker registry
docker build -t your_account/chatgpt-web-front:latest .
docker login
docker push your_account/chatgpt-web-front:latest

```



## 常见问题


Q: 文件保存时全部爆红?

A: `vscode` 请安装项目推荐插件，或手动安装 `Eslint` 插件。

Q: 前端没有打字机效果？

A: 一种可能原因是经过 Nginx 反向代理，开启了 buffer，则 Nginx 会尝试从后端缓冲一定大小的数据再发送给浏览器。请尝试在反代参数后添加 `proxy_buffering off;`，然后重载 Nginx。其他 web server 配置同理。




## License
MIT © [aixiaoxin]
本人公众号：AI小新

参考原项目地址：https://github.com/Chanzhaoyu/chatgpt-web/


## 协议格式

url `/chat-process`

POST请求格式
```json

{
  "options": {},
  "prompt": "chatgpt是什么？",
  "systemMessage": "You are ChatGPT, a large language model trained by OpenAI. Answer as concisely as possible."
}
```
HTTP流式推送(Server-sent events, SSE)响应格式
```json
{
  "role": "assistant",
  "id":"chatcmpl-9PKANwOm1plQPhZN2KxoVPyfCUVpD",
  "parentMessageId":"d2b327d8-ad59-4a01-93a1-a07301763678",
  "text":"ChatGPT的优势包括：\n\n1. **语言理解和生成能力强大**：ChatGPT经过大规模训练，具有出色的语言理解和生成能力。\n   \n2. **多领域知识**：ChatGPT可以回答各种主题的问题，覆盖广泛的领域，如科学、历史、文化等。\n\n3. **实时性**：ChatGPT能够即时提供信息和回答疑问，为用户快速解决问题提供了便利。\n\n4. **24/7可用**：作为一个基于AI的虚拟助手，ChatGPT可以在任何时间为用户提供帮助，无需休息。\n\n5. **个性化交互**：ChatGPT可以根据用户输入偏好进行个性化回复，提供更符合用户需求的信息。\n\n6. **无误差率**：相比于人类，ChatGPT在回答问题时不受疲劳、情绪等因素影响，保持着高效率和准确性。\n\n7. **隐私保护**：ChatGPT是一个匿名的虚拟助手，不会存储对话内容，保护用户隐私。",
  "detail": {
    "id":"chatcmpl-9PKANwOm1plQPhZN2KxoVPyfCUVpD",
    "object":"chat.completion.chunk",
    "created":1715822747,
    "model":"gpt-3.5-turbo-0125",
    "system_fingerprint":null,
    "choices": [
      {"index":0,"delta":{},"logprobs":null,"finish_reason":"stop"}
    ]
  }
} 
```
